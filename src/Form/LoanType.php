<?php

namespace App\Form;

use App\Entity\Loan;
use App\Entity\Ressource;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('created_at')
            ->add('finishied_at')
            // ->add('returned_at')
            ->add('user',EntityType::class, [
                'class' => User::class,
                'choice_label' => function ($user) {
                    return $user->getFirstname().' '.$user->getLastname();
                },
                'multiple' => false,
            ])
            ->add('ressource',EntityType::class, [
                'class' => Ressource::class,
                'choice_label' => 'label',
                'multiple' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Loan::class,
        ]);
    }
}
