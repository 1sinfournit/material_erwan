<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Category;
use App\Entity\Ressource;
use App\Repository\GroupRepository;
use App\Repository\CategoryRepository;
use App\Repository\RessourceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

#[Route('/api', name: 'api')]
class ApiController extends AbstractController
{

    #[Route('/ressource', name: 'ressource',methods:'get')]
    public function ressource(RessourceRepository $categotyRepository, SerializerInterface $serialiser): Response
    {
        // $ressource = $categotyRepository->findAll();
        // $json = $serialiser->serialize($ressource,'json',['ressources'=>'ressource:read']);

        // return new Response($json,200,["content-Type"=>"application/json"],['ressources'=>'ressource:read']);
        return $this->json($categotyRepository->findAll(), 200, [], ['groups' => 'ressource:read']);

    }

    #[Route('/ressource/{id}', name: 'ressource_unique',methods:'get')]
    public function ressource_by_id(RessourceRepository $categotyRepository, Ressource $ressource, SerializerInterface $serialiser): Response
    {
        return $this->json($categotyRepository->find($ressource), 200, [], ['groups' => 'ressource:read']);

    }

    #[Route('/post/ressource', name: 'ressource_post', methods:'Post')]
    public function ressourcepost(Request $request, EntityManagerInterface $em, SerializerInterface $serialiser,ValidatorInterface $validator,GroupRepository $grouprepository,CategoryRepository $categorypository): Response
    {
        $jsonRecu = json_decode($request->getContent());

        if(!empty($jsonRecu->groups)){
            $groupID = $jsonRecu->groups;
            unset($jsonRecu->groups);
        }

        if(!empty($jsonRecu->categorie)){
            $categorieID = $jsonRecu->categorie;
            unset($jsonRecu->categorie);
        }

        try{
            $post = $serialiser->deserialize(json_encode($jsonRecu), Ressource::class, 'json');

            if(!empty($groupID)){
               $groups = $grouprepository->findById($groupID);
                foreach($groups as $group){
                    $post->addGroupe($group);
                }
            }

            if(!empty($categorieID)){
               $categories = $categorypository->findById($categorieID);
                foreach($categories as $category){
                    $post->addCategory($category);
                }
            }
            
            $errors = $validator->validate($post);

            if(count($errors)>0){
                return $this->json($errors, 400);
            }
            $em->persist($post);
            $em->flush();

            return $this->json($post,201,[],['groups'=>'ressource:read']);
        } catch (NotEncodableValueException $e){
            return $this->json([
                'status'=>400,
                'message'=>$e->getMessage()
            ],400);
        }

    }

    #[Route('/patch/ressource/{id}', name: 'ressource_mod', methods:'patch')]
    public function ressource_mod(Request $request, Ressource $ressource,  EntityManagerInterface $em): Response
    {
        $json = json_decode($request->getContent());

        // dd($json);

        if(!empty($json->label)){
            $ressource->setLabel($json->label);
        }

        if(!empty($json->description)){
            $ressource->setDescription($json->description);
        }

        if(!empty($json->quantity_total)){
            $ressource->setQuantityTotal($json->quantity_total);
        }
        
        $em->persist($ressource);
        $em->flush();

        return $this->json($ressource,201,[],['groups'=>'ressource:read']);
    }

    #[Route('/post/ressource/{id}/add/group/{group_id}', name: 'ressource_add_group', methods:'post')]
    #[Entity('group',expr:'repository.find(group_id)')]
    public function ressource_add_group(Ressource $ressource, Group $group,  EntityManagerInterface $em): Response
    {
        // dd($ressource);
        $ressource->addGroupe($group);
        
        $em->persist($ressource);
        $em->flush();

        return $this->json($ressource,201,[],['groups'=>'ressource:read']);
    }

    #[Route('/delete/ressource/{id}/remove/group/{group_id}', name: 'ressource_remove_group', methods:'delete')]
    #[Entity('group',expr:'repository.find(group_id)')]
    public function ressource_remove_group(Ressource $ressource, Group $group,  EntityManagerInterface $em): Response
    {
        // dd($ressource);
        $ressource->removeGroupe($group);
        
        $em->persist($ressource);
        $em->flush();

        return $this->json($ressource,201,[],['groups'=>'ressource:read']);
    }

    #[Route('/post/ressource/{id}/add/category/{category_id}', name: 'ressource_add_category', methods:'post')]
    #[Entity('category',expr:'repository.find(category_id)')]
    public function ressource_add_category(Ressource $ressource, Category $category,  EntityManagerInterface $em): Response
    {
        // dd($ressource);
        $ressource->addCategory($category);
        
        $em->persist($ressource);
        $em->flush();

        return $this->json($ressource,201,[],['groups'=>'ressource:read']);
    }

    #[Route('/delete/ressource/{id}/remove/category/{category_id}', name: 'ressource_remove_category', methods:'delete')]
    #[Entity('category',expr:'repository.find(category_id)')]
    public function ressource_remove_category(Ressource $ressource, category $category,  EntityManagerInterface $em): Response
    {
        // dd($ressource);
        $ressource->removeCategory($category);
        
        $em->persist($ressource);
        $em->flush();
    
        return $this->json($ressource,201,[],['groups'=>'ressource:read']);
    }

    #[Route('/delete/ressource/{id}', name: 'ressource_remove', methods:'delete')]
    public function ressource_remove(Ressource $ressource, EntityManagerInterface $em): Response
    {
        
        $em->remove($ressource);
        $em->flush();
    
        return $this->json($ressource,201,[],['groups'=>'ressource:read']);
    }
}
