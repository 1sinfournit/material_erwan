<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiNoticeController extends AbstractController
{
    #[Route('/notice', name: 'api_notice')]
    public function index(): Response
    {
        return $this->render('api_notice/index.html.twig', [
            'controller_name' => 'ApiNoticeController',
        ]);
    }
}
