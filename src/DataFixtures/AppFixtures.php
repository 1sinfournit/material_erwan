<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public $passwordHasher;
    public function __construct(UserPasswordHasherInterface $password_Hasher)
    {
        $this->passwordHasher = $password_Hasher;
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 20; $i++) {
            $product = new User();
            $product->setfirstname('jean'.$i);
            $product->setlastname('michel');
            $product->setemail('micheljean'.$i.'@gmail.com');
            $product->setPassword($this->passwordHasher->hashPassword($product,'12345678'));
            $manager->persist($product);
        }

        $manager->flush();
    }
}
